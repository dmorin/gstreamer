/*
 * GStreamer gstreamer-baseoverlayrenderer
 * Copyright (C) <1999> Erik Walthinsen <omega@cse.ogi.edu>
 * Copyright (C) <2003> David Schleef <ds@schleef.org>
 * Copyright (C) <2006> Julien Moutte <julien@moutte.net>
 * Copyright (C) <2006> Zeeshan Ali <zeeshan.ali@nokia.com>
 * Copyright (C) <2006-2008> Tim-Philipp Müller <tim centricular net>
 * Copyright (C) <2009> Young-Ho Cha <ganadist@gmail.com>
 * Copyright (C) <2021> Collabora Ltd.
 *
 * gstbaseoverlayrenderer.h
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __GST_BASE_OVERLAY_RENDERER_H__
#define __GST_BASE_OVERLAY_RENDERER_H__

#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/video-overlay-composition.h>
#include <pango/pangocairo.h>

G_BEGIN_DECLS

#define GST_TYPE_BASE_OVERLAY_RENDERER            (gst_base_overlay_renderer_get_type())
#define GST_BASE_OVERLAY_RENDERER(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj),\
                                         GST_TYPE_BASE_OVERLAY_RENDERER, GstBaseOverlayRenderer))
#define GST_BASE_OVERLAY_RENDERER_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass),\
                                         GST_TYPE_BASE_OVERLAY_RENDERER,GstBaseOverlayRendererClass))
#define GST_BASE_OVERLAY_RENDERER_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),\
                                         GST_TYPE_BASE_OVERLAY_RENDERER, GstBaseOverlayRendererClass))
#define GST_IS_BASE_OVERLAY_RENDERER(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj),\
                                         GST_TYPE_BASE_OVERLAY_RENDERER))
#define GST_IS_BASE_OVERLAY_RENDERER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),\
                                         GST_TYPE_BASE_OVERLAY_RENDERER))

typedef struct _GstBaseOverlayRenderer      GstBaseOverlayRenderer;
typedef struct _GstBaseOverlayRendererClass GstBaseOverlayRendererClass;

/**
 * GstBaseOverlayRendererWrapMode:
 * @GST_BASE_OVERLAY_RENDERER_WRAP_MODE_NONE: no wrapping
 * @GST_BASE_OVERLAY_RENDERER_WRAP_MODE_WORD: do word wrapping
 * @GST_BASE_OVERLAY_RENDERER_WRAP_MODE_CHAR: do char wrapping
 * @GST_BASE_OVERLAY_RENDERER_WRAP_MODE_WORD_CHAR: do word and char wrapping
 *
 * Whether to wrap the text and if so how.
 */
typedef enum {
    GST_BASE_OVERLAY_RENDERER_WRAP_MODE_NONE = -1,
    GST_BASE_OVERLAY_RENDERER_WRAP_MODE_WORD = PANGO_WRAP_WORD,
    GST_BASE_OVERLAY_RENDERER_WRAP_MODE_CHAR = PANGO_WRAP_CHAR,
    GST_BASE_OVERLAY_RENDERER_WRAP_MODE_WORD_CHAR = PANGO_WRAP_WORD_CHAR
} GstBaseOverlayRendererWrapMode;

/**
 * GstBaseOverlayRendererLineAlign:
 * @GST_BASE_OVERLAY_RENDERER_LINE_ALIGN_LEFT: lines are left-aligned
 * @GST_BASE_OVERLAY_RENDERER_LINE_ALIGN_CENTER: lines are center-aligned
 * @GST_BASE_OVERLAY_RENDERER_LINE_ALIGN_RIGHT: lines are right-aligned
 *
 * Alignment of text lines relative to each other
 */
typedef enum {
    GST_BASE_OVERLAY_RENDERER_LINE_ALIGN_LEFT = PANGO_ALIGN_LEFT,
    GST_BASE_OVERLAY_RENDERER_LINE_ALIGN_CENTER = PANGO_ALIGN_CENTER,
    GST_BASE_OVERLAY_RENDERER_LINE_ALIGN_RIGHT = PANGO_ALIGN_RIGHT
} GstBaseOverlayRendererLineAlign;

/**
 * GstBaseOverlayRendererScaleMode:
 * @GST_BASE_OVERLAY_RENDERER_SCALE_MODE_NONE: no compensation
 * @GST_BASE_OVERLAY_RENDERER_SCALE_MODE_PAR: compensate pixel-aspect-ratio scaling
 * @GST_BASE_OVERLAY_RENDERER_SCALE_MODE_DISPLAY: compensate for scaling to display (as determined by overlay allocation meta)
 * @GST_BASE_OVERLAY_RENDERER_SCALE_MODE_USER: compensate scaling set by #GstBaseOverlayRenderer:scale-pixel-aspect-ratio property
 *
 * Scale text to compensate for and avoid aspect distortion by subsequent
 * scaling of video
 */
typedef enum {
    GST_BASE_OVERLAY_RENDERER_SCALE_MODE_NONE,
    GST_BASE_OVERLAY_RENDERER_SCALE_MODE_PAR,
    GST_BASE_OVERLAY_RENDERER_SCALE_MODE_DISPLAY,
    GST_BASE_OVERLAY_RENDERER_SCALE_MODE_USER
} GstBaseOverlayRendererScaleMode;

/**
 * GstBaseOverlayRenderer:
 *
 * Opaque textoverlay object structure
 */
struct _GstBaseOverlayRenderer {
    GstElement               element;

    GstPad                  *sinkpad;
    GstPad                  *srcpad;

    PangoContext            *pango_context;

    GstSegment               segment;
    gboolean                 flushing;
    gboolean                 eos;

    GMutex                   lock;
    GCond                    cond;  /* to signal removal of a queued text
                                     * buffer, arrival of a text buffer,
                                     * a text segment update, or a change
                                     * in status (e.g. shutdown, flushing) */
    /* stream metrics */
    GstVideoInfo             info;
    GstVideoFormat           format;
    gint                     width;
    gint                     height;

    /* properties */
    gint                     xpad;
    gint                     ypad;
    gint                     deltax;
    gint                     deltay;
    gdouble                  xpos;
    gdouble                  ypos;
    PangoLayout             *layout;
    guint                    rect_color;
    guint                    text_color, text_outline_color;
    gboolean                 auto_adjust_size;
    gboolean                 draw_outline;
    gboolean                 use_vertical_render;
    GstBaseOverlayRendererWrapMode   wrap_mode;
    GstBaseOverlayRendererLineAlign  line_align;
    GstBaseOverlayRendererScaleMode  scale_mode;
    gint                     scale_par_n;
    gint                     scale_par_d;

    /* dimension relative to which the render is done, this is the stream size
     * or a portion of the window_size (adapted to aspect ratio) */
    gint                     render_width;
    gint                     render_height;
    /* This is (render_width / width) uses to convert to stream scale */
    gdouble                  render_scale;

    /* window dimension, reported in the composition meta params. This is set
     * to stream width, height if missing */
    gint                     window_width;
    gint                     window_height;

    gdouble                  shadow_offset;
    gdouble                  outline_offset;

    PangoRectangle           ink_rect;
    PangoRectangle           logical_rect;

    /* overlay composition */
    gboolean                 attach_compo_to_buffer;
    GstVideoOverlayComposition *composition;
    GstBuffer               *composition_buffer;
    GstVideoOverlayComposition *upstream_composition;

    /* dimension of composition_buffer, the physical dimension */
    guint                    composition_buffer_width;
    guint                    composition_buffer_height;
    gchar                   *text;
};

struct _GstBaseOverlayRendererClass {
    GstElementClass parent_class;
};

GType gst_base_overlay_renderer_get_type(void) G_GNUC_CONST;

G_END_DECLS

#endif /* __GST_BASE_OVERLAY_RENDERER_H */
