/*
 * GStreamer gstreamer-baseoverlayrenderer
 * Copyright (C) <1999> Erik Walthinsen <omega@cse.ogi.edu>
 * Copyright (C) <2003> David Schleef <ds@schleef.org>
 * Copyright (C) <2006> Julien Moutte <julien@moutte.net>
 * Copyright (C) <2006> Zeeshan Ali <zeeshan.ali@nokia.com>
 * Copyright (C) <2006-2008> Tim-Philipp Müller <tim centricular net>
 * Copyright (C) <2009> Young-Ho Cha <ganadist@gmail.com>
 * Copyright (C) <2021> Collabora Ltd.
 *
 * gstbaseoverlayrenderer.c
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gst/video/video.h>
#include <gst/video/gstvideometa.h>

#include "gstbaseoverlayrenderer.h"
#include "gstonnxoverlayrenderer.h"
#include "gstonnxobjectdetector.h"
#include <gst/analyticmeta/generic/gstanalysismeta.h>
#include <gst/analyticmeta/classification/gstanalysisclassificationmtd.h>
#include <gst/analyticmeta/object_detection/gstobjectdetectionmtd.h>
#include <gst/analyticmeta/tracking/gstobjecttrackingmtd.h>
#include <string.h>
#include <math.h>

typedef struct _GstOnnxOverlayGraphicsShape GstOnnxOverlayGraphicsShape;
typedef struct _GstOnnxOverlayGraphicsText GstOnnxOverlayGraphicsText;
typedef struct _GstOnnxOverlayGraphicsRect GstOnnxOverlayGraphicsRect;

GST_DEBUG_CATEGORY_STATIC (onnx_overlay_renderer_debug);
#define GST_CAT_DEFAULT onnx_overlay_renderer_debug

/**
 * GstOnnxOverlayGraphicsShapeEnum
 * @GST_ONNX_OVERLAY_GRAPHICS_SHAPE: generic shape
 * @GST_ONNX_OVERLAY_GRAPHICS_SHAPE_TEXT: text shape
 * @GST_ONNX_OVERLAY_GRAPHICS_SHAPE_RECT: rectangular shape
 *
 * Type of overlay shape
 *
 * Since: 1.20
 */
typedef enum
{
  /* generic shape */
  GST_ONNX_OVERLAY_GRAPHICS_SHAPE,

  /* text shape */
  GST_ONNX_OVERLAY_GRAPHICS_SHAPE_TEXT,

  /* rectangular shape */
  GST_ONNX_OVERLAY_GRAPHICS_SHAPE_RECT
} GstOnnxOverlayGraphicsShapeEnum;

/* ABGR overlay color */
typedef guint GST_ONNX_OVERLAY_GRAPHICS_COLOR;

/**
 * GstOnnxOverlayGraphicsShape:
 * @parent: parent #GstOnnxOverlayGraphicsShape
 * @type: #GstOnnxOverlayGraphicsShapeEnum specifying type of shape
 * @id: shape's unique identifier
 * @zorder: rendering z order. Shapes with larger z order will be rendered
 * in front of shapes with smaller z order
 * @x: top left x coordinate of text
 * @y: top left y coordinate of text
 * @width: width of bounding box
 * @height: height of bounding box
 * @fill_color: AGBR value of font color
 * @stroke_width: font stroke size
 * @stroke_color: AGBR value of stroke color
 * @children: child shapes
 *
 * Meta-data related to overlay shape
 *
 * Since: 1.20
 */
struct _GstOnnxOverlayGraphicsShape
{
  GstOnnxOverlayGraphicsShape *parent;
  GstOnnxOverlayGraphicsShapeEnum type;
  guint32 id;
  gint zorder;
  gint x, y;
  guint width, height;
  GST_ONNX_OVERLAY_GRAPHICS_COLOR fill_color;
  guint stroke_width;
  GST_ONNX_OVERLAY_GRAPHICS_COLOR stroke_color;
  GHashTable *children;
};

/**
 * GstOnnxOverlayGraphicsText:
 * @shape: parent shape
 * @text: text contents
 * @font_desc: string formatted as [FAMILY-LIST] [STYLE-OPTIONS]
 * where FAMILY-LIST is a comma separated list of families optionally terminated
 * by a comma, and STYLE_OPTIONS is a whitespace separated list of words
 * where each WORD describes one of style, variant, weight, or stretch
 * Examples:
 * "sans bold"
 * "serif,monospace bold italic condensed"
 * "normal"
 * @font_size: size of font
 *
 * Meta-data related to overlay text
 *
 * Since: 1.20
 */
struct _GstOnnxOverlayGraphicsText
{
  GstOnnxOverlayGraphicsShape shape;
  const gchar *text;
  const gchar *font_desc;
  guint font_size;
};

/**
 * GstOnnxOverlayGraphicsRect:
 * @shape: parent shape
 *
 * Meta-data related to overlay rectangle
 *
 * Since: 1.20
 */
struct _GstOnnxOverlayGraphicsRect
{
  GstOnnxOverlayGraphicsShape shape;
};


/* FIXME:
 *  - use proper strides and offset for I420
 *  - if text is wider than the video picture, it does not get
 *    clipped properly during blitting (if wrapping is disabled)
 */

#define DEFAULT_PROP_SHADING	FALSE
#define DEFAULT_PROP_XPAD	25
#define DEFAULT_PROP_YPAD	25
#define DEFAULT_PROP_DELTAX	0
#define DEFAULT_PROP_DELTAY	0
#define DEFAULT_PROP_XPOS       0.5
#define DEFAULT_PROP_YPOS       0.5
#define DEFAULT_PROP_WRAP_MODE  GST_BASE_OVERLAY_RENDERER_WRAP_MODE_WORD_CHAR
#define DEFAULT_PROP_FONT_DESC	""
#define DEFAULT_PROP_LINE_ALIGNMENT GST_BASE_OVERLAY_RENDERER_LINE_ALIGN_CENTER
#define DEFAULT_PROP_WAIT_TEXT	TRUE
#define DEFAULT_PROP_AUTO_ADJUST_SIZE TRUE
#define DEFAULT_PROP_VERTICAL_RENDER  FALSE
#define DEFAULT_PROP_SCALE_MODE GST_BASE_OVERLAY_RENDERER_SCALE_MODE_NONE
#define DEFAULT_PROP_SCALE_PAR_N 1
#define DEFAULT_PROP_SCALE_PAR_D 1
#define DEFAULT_PROP_DRAW_SHADOW TRUE
#define DEFAULT_PROP_DRAW_OUTLINE TRUE
#define DEFAULT_PROP_RECT_COLOR      0xffffffff
#define DEFAULT_PROP_TEXT_COLOR      0xffffffff
#define DEFAULT_PROP_TEXT_OUTLINE_COLOR    0xff000000
#define DEFAULT_PROP_TEXT_X 0
#define DEFAULT_PROP_TEXT_Y 0
#define DEFAULT_PROP_TEXT_WIDTH 1
#define DEFAULT_PROP_TEXT_HEIGHT 1
#define MINIMUM_OUTLINE_OFFSET 1.0
#define DEFAULT_SCALE_BASIS    640

enum
{
  PROP_0,
  PROP_RECT_COLOR,
  PROP_TEXT_COLOR,
  PROP_TEXT_OUTLINE_COLOR,
  PROP_LAST
};

#define VIDEO_FORMATS GST_VIDEO_OVERLAY_COMPOSITION_BLEND_FORMATS
#define BASE_OVERLAY_RENDERER_CAPS GST_VIDEO_CAPS_MAKE (VIDEO_FORMATS)
#define BASE_OVERLAY_RENDERER_ALL_CAPS BASE_OVERLAY_RENDERER_CAPS ";" \
    GST_VIDEO_CAPS_MAKE_WITH_FEATURES ("ANY", GST_VIDEO_FORMATS_ALL)
static GstStaticCaps sw_template_caps =
GST_STATIC_CAPS (BASE_OVERLAY_RENDERER_CAPS);

static GstStaticPadTemplate src_template_factory =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (BASE_OVERLAY_RENDERER_ALL_CAPS)
    );

static GstStaticPadTemplate video_sink_template_factory =
GST_STATIC_PAD_TEMPLATE ("video_sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (BASE_OVERLAY_RENDERER_ALL_CAPS)
    );

#define GST_BASE_OVERLAY_RENDERER_GET_LOCK(ov) (&GST_BASE_OVERLAY_RENDERER (ov)->lock)
#define GST_BASE_OVERLAY_RENDERER_GET_COND(ov) (&GST_BASE_OVERLAY_RENDERER (ov)->cond)
#define GST_BASE_OVERLAY_RENDERER_LOCK(ov)     (g_mutex_lock (GST_BASE_OVERLAY_RENDERER_GET_LOCK (ov)))
#define GST_BASE_OVERLAY_RENDERER_UNLOCK(ov)   (g_mutex_unlock (GST_BASE_OVERLAY_RENDERER_GET_LOCK (ov)))
#define GST_BASE_OVERLAY_RENDERER_WAIT(ov)     (g_cond_wait (GST_BASE_OVERLAY_RENDERER_GET_COND (ov), GST_BASE_OVERLAY_RENDERER_GET_LOCK (ov)))
#define GST_BASE_OVERLAY_RENDERER_SIGNAL(ov)   (g_cond_signal (GST_BASE_OVERLAY_RENDERER_GET_COND (ov)))
#define GST_BASE_OVERLAY_RENDERER_BROADCAST(ov)(g_cond_broadcast (GST_BASE_OVERLAY_RENDERER_GET_COND (ov)))

static GstElementClass *parent_class = NULL;
static void gst_base_overlay_renderer_class_init (GstBaseOverlayRendererClass *
    klass);
static void gst_base_overlay_renderer_init (GstBaseOverlayRenderer * overlay,
    GstBaseOverlayRendererClass * klass);
static GstStateChangeReturn gst_base_overlay_renderer_change_state (GstElement *
    element, GstStateChange transition);
static GstCaps *gst_base_overlay_renderer_get_videosink_caps (GstPad * pad,
    GstBaseOverlayRenderer * overlay, GstCaps * filter);
static GstCaps *gst_base_overlay_renderer_get_src_caps (GstPad * pad,
    GstBaseOverlayRenderer * overlay, GstCaps * filter);
static gboolean gst_base_overlay_renderer_setcaps (GstBaseOverlayRenderer *
    overlay, GstCaps * caps);
static gboolean gst_base_overlay_renderer_src_event (GstPad * pad,
    GstObject * parent, GstEvent * event);
static gboolean gst_base_overlay_renderer_src_query (GstPad * pad,
    GstObject * parent, GstQuery * query);
static gboolean gst_base_overlay_renderer_video_event (GstPad * pad,
    GstObject * parent, GstEvent * event);
static gboolean gst_base_overlay_renderer_video_query (GstPad * pad,
    GstObject * parent, GstQuery * query);
static GstFlowReturn gst_base_overlay_renderer_video_chain (GstPad * pad,
    GstObject * parent, GstBuffer * buffer);
static void gst_base_overlay_renderer_finalize (GObject * object);
static void gst_base_overlay_renderer_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec);
static void gst_base_overlay_renderer_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);
static void
gst_base_overlay_renderer_adjust_values_with_fontdesc (GstBaseOverlayRenderer *
    overlay, PangoFontDescription * desc);
static gboolean gst_base_overlay_renderer_can_handle_caps (GstCaps * incaps);
static void
gst_base_overlay_renderer_update_render_size (GstBaseOverlayRenderer * overlay);
#define GST_TYPE_BASE_OVERLAY_RENDERER_WRAP_MODE (gst_base_overlay_renderer_wrap_mode_get_type())
static GType gst_base_overlay_renderer_wrap_mode_get_type (void);
#define GST_TYPE_BASE_OVERLAY_RENDERER_LINE_ALIGN (gst_base_overlay_renderer_line_align_get_type())
static GType gst_base_overlay_renderer_line_align_get_type (void);
#define GST_TYPE_BASE_OVERLAY_RENDERER_SCALE_MODE (gst_base_overlay_renderer_scale_mode_get_type())
static GType gst_base_overlay_renderer_scale_mode_get_type (void);
static void gst_base_overlay_renderer_get_pos (GstBaseOverlayRenderer * overlay,
    gint * xpos, gint * ypos);
static gboolean gst_base_overlay_renderer_render (GstBaseOverlayRenderer *
    overlay, GstBuffer * buf);
static void
gst_base_overlay_renderer_render_text (GstBaseOverlayRenderer * overlay,
    GstOnnxOverlayGraphicsShape * shape, const gchar * string, gint textlen);
static void gst_base_overlay_renderer_render_rect (GstBaseOverlayRenderer *
    overlay, GstOnnxOverlayGraphicsShape * shape);

static void
gst_base_overlay_renderer_class_init (GstBaseOverlayRendererClass * klass)
{
  GObjectClass *gobject_class;
  GstElementClass *gstelement_class;

  gobject_class = (GObjectClass *) klass;
  gstelement_class = (GstElementClass *) klass;

  GST_DEBUG_CATEGORY_INIT (onnx_overlay_renderer_debug, "onnxoverlayrenderer",
      0, "Onnx overlay renderer");

  parent_class = g_type_class_peek_parent (klass);

  gobject_class->finalize = gst_base_overlay_renderer_finalize;
  gobject_class->set_property = gst_base_overlay_renderer_set_property;
  gobject_class->get_property = gst_base_overlay_renderer_get_property;

  gst_element_class_add_static_pad_template (gstelement_class,
      &src_template_factory);
  gst_element_class_add_static_pad_template (gstelement_class,
      &video_sink_template_factory);

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_base_overlay_renderer_change_state);

  /**
   * GstBaseOverlayRenderer:text-color:
   *
   * Color of the rendered text.
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_TEXT_COLOR,
      g_param_spec_uint ("text-color", "Text color",
          "Color to use for text (big-endian ARGB).", 0, G_MAXUINT32,
          DEFAULT_PROP_TEXT_COLOR,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));
  /**
   * GstBaseOverlayRenderer:text-outline-color:
   *
   * Color of the outline of the rendered text.
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_TEXT_OUTLINE_COLOR, g_param_spec_uint ("text-outline-color",
          "Text Outline Color",
          "Color to use for outline the text (big-endian ARGB).", 0,
          G_MAXUINT32, DEFAULT_PROP_TEXT_OUTLINE_COLOR,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));

  /**
   * GstBaseOverlayRenderer:rect-color:
   *
   * Color of the rendered rectangle.
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_RECT_COLOR,
      g_param_spec_uint ("rect-color", "Rectangle color",
          "Color to use for rectangle (big-endian ARGB).", 0, G_MAXUINT32,
          DEFAULT_PROP_RECT_COLOR,
          G_PARAM_READWRITE | GST_PARAM_CONTROLLABLE | G_PARAM_STATIC_STRINGS));

  gst_type_mark_as_plugin_api (GST_TYPE_BASE_OVERLAY_RENDERER_LINE_ALIGN, 0);
  gst_type_mark_as_plugin_api (GST_TYPE_BASE_OVERLAY_RENDERER_SCALE_MODE, 0);
  gst_type_mark_as_plugin_api (GST_TYPE_BASE_OVERLAY_RENDERER_WRAP_MODE, 0);
  gst_type_mark_as_plugin_api (GST_TYPE_BASE_OVERLAY_RENDERER, 0);
}

static void
gst_base_overlay_renderer_finalize (GObject * object)
{
  GstBaseOverlayRenderer *overlay = GST_BASE_OVERLAY_RENDERER (object);

  g_free (overlay->text);

  if (overlay->composition) {
    gst_video_overlay_composition_unref (overlay->composition);
    overlay->composition = NULL;
  }

  if (overlay->composition_buffer) {
    gst_buffer_unref (overlay->composition_buffer);
    overlay->composition_buffer = NULL;
  }

  if (overlay->layout) {
    g_object_unref (overlay->layout);
    overlay->layout = NULL;
  }

  if (overlay->pango_context) {
    g_object_unref (overlay->pango_context);
    overlay->pango_context = NULL;
  }

  g_mutex_clear (&overlay->lock);
  g_cond_clear (&overlay->cond);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_base_overlay_renderer_init (GstBaseOverlayRenderer * overlay,
    GstBaseOverlayRendererClass * klass)
{
  GstPadTemplate *template;
  PangoFontDescription *desc;
  PangoFontMap *fontmap;

  fontmap = pango_cairo_font_map_new ();
  overlay->pango_context =
      pango_font_map_create_context (PANGO_FONT_MAP (fontmap));
  g_object_unref (fontmap);
  pango_context_set_base_gravity (overlay->pango_context, PANGO_GRAVITY_SOUTH);

  /* sink */
  template = gst_static_pad_template_get (&video_sink_template_factory);
  overlay->sinkpad = gst_pad_new_from_template (template, "sink");
  gst_object_unref (template);
  gst_pad_set_event_function (overlay->sinkpad,
      GST_DEBUG_FUNCPTR (gst_base_overlay_renderer_video_event));
  gst_pad_set_chain_function (overlay->sinkpad,
      GST_DEBUG_FUNCPTR (gst_base_overlay_renderer_video_chain));
  gst_pad_set_query_function (overlay->sinkpad,
      GST_DEBUG_FUNCPTR (gst_base_overlay_renderer_video_query));
  GST_PAD_SET_PROXY_ALLOCATION (overlay->sinkpad);
  gst_element_add_pad (GST_ELEMENT (overlay), overlay->sinkpad);

  /* source */
  template = gst_static_pad_template_get (&src_template_factory);
  overlay->srcpad = gst_pad_new_from_template (template, "src");
  gst_object_unref (template);
  gst_pad_set_event_function (overlay->srcpad,
      GST_DEBUG_FUNCPTR (gst_base_overlay_renderer_src_event));
  gst_pad_set_query_function (overlay->srcpad,
      GST_DEBUG_FUNCPTR (gst_base_overlay_renderer_src_query));
  gst_element_add_pad (GST_ELEMENT (overlay), overlay->srcpad);

  overlay->layout = pango_layout_new (overlay->pango_context);
  desc = pango_context_get_font_description (overlay->pango_context);
  gst_base_overlay_renderer_adjust_values_with_fontdesc (overlay, desc);

  overlay->xpad = DEFAULT_PROP_XPAD;
  overlay->ypad = DEFAULT_PROP_YPAD;
  overlay->deltax = DEFAULT_PROP_DELTAX;
  overlay->deltay = DEFAULT_PROP_DELTAY;
  overlay->xpos = DEFAULT_PROP_XPOS;
  overlay->ypos = DEFAULT_PROP_YPOS;
  overlay->wrap_mode = DEFAULT_PROP_WRAP_MODE;
  overlay->draw_outline = DEFAULT_PROP_DRAW_OUTLINE;
  overlay->rect_color = DEFAULT_PROP_RECT_COLOR;
  overlay->text_color = DEFAULT_PROP_TEXT_COLOR;
  overlay->text_outline_color = DEFAULT_PROP_TEXT_OUTLINE_COLOR;
  overlay->auto_adjust_size = DEFAULT_PROP_AUTO_ADJUST_SIZE;
  overlay->composition_buffer = NULL;
  overlay->use_vertical_render = DEFAULT_PROP_VERTICAL_RENDER;
  overlay->scale_mode = DEFAULT_PROP_SCALE_MODE;
  overlay->scale_par_n = DEFAULT_PROP_SCALE_PAR_N;
  overlay->scale_par_d = DEFAULT_PROP_SCALE_PAR_D;
  overlay->line_align = DEFAULT_PROP_LINE_ALIGNMENT;
  pango_layout_set_alignment (overlay->layout,
      (PangoAlignment) overlay->line_align);
  overlay->composition = NULL;
  overlay->upstream_composition = NULL;
  overlay->width = 1;
  overlay->height = 1;
  overlay->window_width = 1;
  overlay->window_height = 1;
  overlay->composition_buffer_width = DEFAULT_PROP_TEXT_WIDTH;
  overlay->composition_buffer_height = DEFAULT_PROP_TEXT_HEIGHT;
  overlay->render_width = 1;
  overlay->render_height = 1;
  overlay->render_scale = 1.0l;

  g_mutex_init (&overlay->lock);
  g_cond_init (&overlay->cond);
  gst_segment_init (&overlay->segment, GST_FORMAT_TIME);
}

static void
gst_base_overlay_renderer_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstBaseOverlayRenderer *overlay = GST_BASE_OVERLAY_RENDERER (object);

  GST_BASE_OVERLAY_RENDERER_LOCK (overlay);
  switch (prop_id) {
    case PROP_TEXT_COLOR:
      overlay->text_color = g_value_get_uint (value);
      break;
    case PROP_TEXT_OUTLINE_COLOR:
      overlay->text_outline_color = g_value_get_uint (value);
      break;
    case PROP_RECT_COLOR:
      overlay->rect_color = g_value_get_uint (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }

  GST_BASE_OVERLAY_RENDERER_UNLOCK (overlay);
}

static void
gst_base_overlay_renderer_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstBaseOverlayRenderer *overlay = GST_BASE_OVERLAY_RENDERER (object);

  GST_BASE_OVERLAY_RENDERER_LOCK (overlay);
  switch (prop_id) {
    case PROP_TEXT_COLOR:
      g_value_set_uint (value, overlay->text_color);
      break;
    case PROP_TEXT_OUTLINE_COLOR:
      g_value_set_uint (value, overlay->text_outline_color);
      break;
    case PROP_RECT_COLOR:
      g_value_set_uint (value, overlay->rect_color);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }

  GST_BASE_OVERLAY_RENDERER_UNLOCK (overlay);
}

/* CAPS NEGOTIATION  ************/

/* only negotiate/query video overlay composition support for now */
static gboolean
gst_base_overlay_renderer_negotiate (GstBaseOverlayRenderer * overlay,
    GstCaps * caps)
{
  gboolean upstream_has_meta = FALSE;
  gboolean caps_has_meta = FALSE;
  gboolean alloc_has_meta = FALSE;
  gboolean attach = FALSE;
  gboolean ret = TRUE;
  guint width, height;
  GstCapsFeatures *f;
  GstCaps *overlay_caps;
  GstQuery *query;
  guint alloc_index;

  GST_DEBUG_OBJECT (overlay, "performing negotiation");

  /* Clear any pending reconfigure to avoid negotiating twice */
  gst_pad_check_reconfigure (overlay->srcpad);

  if (!caps)
    caps = gst_pad_get_current_caps (overlay->sinkpad);
  else
    gst_caps_ref (caps);

  if (!caps || gst_caps_is_empty (caps))
    goto no_format;

  /* Check if upstream caps have meta */
  if ((f = gst_caps_get_features (caps, 0))) {
    upstream_has_meta = gst_caps_features_contains (f,
        GST_CAPS_FEATURE_META_GST_VIDEO_OVERLAY_COMPOSITION);
  }

  /* Initialize dimensions */
  width = overlay->width;
  height = overlay->height;

  if (upstream_has_meta) {
    overlay_caps = gst_caps_ref (caps);
  } else {
    GstCaps *peercaps;

    /* BaseTransform requires caps for the allocation query to work */
    overlay_caps = gst_caps_copy (caps);
    f = gst_caps_get_features (overlay_caps, 0);
    gst_caps_features_add (f,
        GST_CAPS_FEATURE_META_GST_VIDEO_OVERLAY_COMPOSITION);

    /* Then check if downstream accept overlay composition in caps */
    /* FIXME: We should probably check if downstream *prefers* the
     * overlay meta, and only enforce usage of it if we can't handle
     * the format ourselves and thus would have to drop the overlays.
     * Otherwise we should prefer what downstream wants here.
     */
    peercaps = gst_pad_peer_query_caps (overlay->srcpad, overlay_caps);
    caps_has_meta = !gst_caps_is_empty (peercaps);
    gst_caps_unref (peercaps);

    GST_DEBUG_OBJECT (overlay, "caps have overlay meta %d", caps_has_meta);
  }

  if (upstream_has_meta || caps_has_meta) {
    /* Send caps immediately, it's needed by GstBaseTransform to get a reply
     * from allocation query */
    ret = gst_pad_set_caps (overlay->srcpad, overlay_caps);

    /* First check if the allocation meta has compositon */
    query = gst_query_new_allocation (overlay_caps, FALSE);

    if (!gst_pad_peer_query (overlay->srcpad, query)) {
      /* no problem, we use the query defaults */
      GST_DEBUG_OBJECT (overlay, "ALLOCATION query failed");

      /* In case we were flushing, mark reconfigure and fail this method,
       * will make it retry */
      if (overlay->flushing)
        ret = FALSE;
    }

    alloc_has_meta = gst_query_find_allocation_meta (query,
        GST_VIDEO_OVERLAY_COMPOSITION_META_API_TYPE, &alloc_index);

    GST_DEBUG_OBJECT (overlay, "sink alloc has overlay meta %d",
        alloc_has_meta);

    if (alloc_has_meta) {
      const GstStructure *params;

      gst_query_parse_nth_allocation_meta (query, alloc_index, &params);
      if (params) {
        if (gst_structure_get (params, "width", G_TYPE_UINT, &width,
                "height", G_TYPE_UINT, &height, NULL)) {
          GST_DEBUG_OBJECT (overlay, "received window size: %dx%d", width,
              height);
          g_assert (width != 0 && height != 0);
        }
      }
    }

    gst_query_unref (query);
  }

  /* Update render size if needed */
  overlay->window_width = width;
  overlay->window_height = height;
  gst_base_overlay_renderer_update_render_size (overlay);

  /* For backward compatibility, we will prefer blitting if downstream
   * allocation does not support the meta. In other case we will prefer
   * attaching, and will fail the negotiation in the unlikely case we are
   * force to blit, but format isn't supported. */

  if (upstream_has_meta) {
    attach = TRUE;
  } else if (caps_has_meta) {
    if (alloc_has_meta) {
      attach = TRUE;
    } else {
      /* Don't attach unless we cannot handle the format */
      attach = !gst_base_overlay_renderer_can_handle_caps (caps);
    }
  } else {
    ret = gst_base_overlay_renderer_can_handle_caps (caps);
  }

  /* If we attach, then pick the overlay caps */
  if (attach) {
    GST_DEBUG_OBJECT (overlay, "Using caps %" GST_PTR_FORMAT, overlay_caps);
    /* Caps where already sent */
  } else if (ret) {
    GST_DEBUG_OBJECT (overlay, "Using caps %" GST_PTR_FORMAT, caps);
    ret = gst_pad_set_caps (overlay->srcpad, caps);
  }

  overlay->attach_compo_to_buffer = attach;

  if (!ret) {
    GST_DEBUG_OBJECT (overlay, "negotiation failed, schedule reconfigure");
    gst_pad_mark_reconfigure (overlay->srcpad);
  }

  gst_caps_unref (overlay_caps);
  gst_caps_unref (caps);

  return ret;

no_format:
  {
    if (caps)
      gst_caps_unref (caps);
    gst_pad_mark_reconfigure (overlay->srcpad);
    return FALSE;
  }
}

static gboolean
gst_base_overlay_renderer_can_handle_caps (GstCaps * incaps)
{
  gboolean ret;
  GstCaps *caps;

  caps = gst_static_caps_get (&sw_template_caps);
  ret = gst_caps_is_subset (incaps, caps);
  gst_caps_unref (caps);

  return ret;
}

static inline void
gst_base_overlay_renderer_set_composition (GstBaseOverlayRenderer * overlay,
    GstOnnxOverlayGraphicsShape * shape)
{
  gint xpos, ypos;
  GstVideoOverlayRectangle *rectangle;

  if (overlay->composition_buffer && overlay->composition_buffer_width != 1) {
    gint render_width, render_height;

    if (shape) {
      xpos = shape->x;
      ypos = shape->y;
    } else {
      gst_base_overlay_renderer_get_pos (overlay, &xpos, &ypos);
    }

    if (overlay->text) {
      render_width = overlay->ink_rect.width;
      render_height = overlay->ink_rect.height;
    } else {
      render_width = overlay->composition_buffer_width;
      render_height = overlay->composition_buffer_height;
    }

    GST_DEBUG_OBJECT (overlay,
        "updating composition for '%s' with window size %dx%d, "
        "buffer size %dx%d, render size %dx%d and position (%d, %d)",
        overlay->text, overlay->window_width, overlay->window_height,
        overlay->composition_buffer_width, overlay->composition_buffer_height,
        render_width, render_height, xpos, ypos);

    gst_buffer_add_video_meta (overlay->composition_buffer,
        GST_VIDEO_FRAME_FLAG_NONE, GST_VIDEO_OVERLAY_COMPOSITION_FORMAT_RGB,
        overlay->composition_buffer_width, overlay->composition_buffer_height);

    rectangle =
        gst_video_overlay_rectangle_new_raw (overlay->composition_buffer, xpos,
        ypos, render_width, render_height,
        GST_VIDEO_OVERLAY_FORMAT_FLAG_PREMULTIPLIED_ALPHA);

    if (overlay->upstream_composition) {
      overlay->composition =
          gst_video_overlay_composition_copy (overlay->upstream_composition);
      gst_video_overlay_composition_add_rectangle (overlay->composition,
          rectangle);
    } else {
      if (!overlay->composition)
        overlay->composition = gst_video_overlay_composition_new (rectangle);
      else
        gst_video_overlay_composition_add_rectangle (overlay->composition,
            rectangle);
    }

    gst_video_overlay_rectangle_unref (rectangle);

  } else if (overlay->composition) {
    gst_video_overlay_composition_unref (overlay->composition);
    overlay->composition = NULL;
  }
}

static gboolean
gst_base_overlay_renderer_setcaps (GstBaseOverlayRenderer * overlay,
    GstCaps * caps)
{
  GstVideoInfo info;
  gboolean ret = FALSE;

  if (!gst_video_info_from_caps (&info, caps))
    goto invalid_caps;

  /* Render again if size have changed */
  if (GST_VIDEO_INFO_WIDTH (&info) != GST_VIDEO_INFO_WIDTH (&overlay->info) ||
      GST_VIDEO_INFO_HEIGHT (&info) != GST_VIDEO_INFO_HEIGHT (&overlay->info))
    overlay->info = info;
  overlay->format = GST_VIDEO_INFO_FORMAT (&info);
  overlay->width = GST_VIDEO_INFO_WIDTH (&info);
  overlay->height = GST_VIDEO_INFO_HEIGHT (&info);

  ret = gst_base_overlay_renderer_negotiate (overlay, caps);

  GST_BASE_OVERLAY_RENDERER_LOCK (overlay);

  if (!overlay->attach_compo_to_buffer &&
      !gst_base_overlay_renderer_can_handle_caps (caps)) {
    GST_DEBUG_OBJECT (overlay, "unsupported caps %" GST_PTR_FORMAT, caps);
    ret = FALSE;
  }
  GST_BASE_OVERLAY_RENDERER_UNLOCK (overlay);

  return ret;

  /* ERRORS */
invalid_caps:
  {
    GST_DEBUG_OBJECT (overlay, "could not parse caps");
    return FALSE;
  }
}

static gboolean
gst_base_overlay_renderer_src_query (GstPad * pad, GstObject * parent,
    GstQuery * query)
{
  gboolean ret = FALSE;
  GstBaseOverlayRenderer *overlay;

  overlay = GST_BASE_OVERLAY_RENDERER (parent);

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_CAPS:
    {
      GstCaps *filter, *caps;

      gst_query_parse_caps (query, &filter);
      caps = gst_base_overlay_renderer_get_src_caps (pad, overlay, filter);
      gst_query_set_caps_result (query, caps);
      gst_caps_unref (caps);
      ret = TRUE;
      break;
    }
    default:
      ret = gst_pad_query_default (pad, parent, query);
      break;
  }

  return ret;
}

static GstFlowReturn
gst_base_overlay_renderer_push_frame (GstBaseOverlayRenderer * overlay,
    GstBuffer * video_frame)
{
  GstVideoFrame frame;

  if (overlay->composition == NULL)
    goto done;

  if (gst_pad_check_reconfigure (overlay->srcpad)) {
    if (!gst_base_overlay_renderer_negotiate (overlay, NULL)) {
      gst_pad_mark_reconfigure (overlay->srcpad);
      gst_buffer_unref (video_frame);
      if (GST_PAD_IS_FLUSHING (overlay->srcpad))
        return GST_FLOW_FLUSHING;
      else
        return GST_FLOW_NOT_NEGOTIATED;
    }
  }

  video_frame = gst_buffer_make_writable (video_frame);

  if (overlay->attach_compo_to_buffer) {
    GST_DEBUG_OBJECT (overlay, "Attaching text overlay image to video buffer");
    gst_buffer_add_video_overlay_composition_meta (video_frame,
        overlay->composition);
    /* FIXME: emulate shaded background box if want_shading=true */
    goto done;
  }

  if (!gst_video_frame_map (&frame, &overlay->info, video_frame,
          GST_MAP_READWRITE))
    goto invalid_frame;

  gst_video_overlay_composition_blend (overlay->composition, &frame);
  gst_video_frame_unmap (&frame);

done:

  return gst_pad_push (overlay->srcpad, video_frame);

  /* ERRORS */
invalid_frame:
  {
    gst_buffer_unref (video_frame);
    GST_DEBUG_OBJECT (overlay, "received invalid buffer");
    return GST_FLOW_OK;
  }
}

static gboolean
gst_base_overlay_renderer_video_event (GstPad * pad, GstObject * parent,
    GstEvent * event)
{
  gboolean ret = FALSE;
  GstBaseOverlayRenderer *overlay = NULL;

  overlay = GST_BASE_OVERLAY_RENDERER (parent);

  GST_DEBUG_OBJECT (pad, "received event %s", GST_EVENT_TYPE_NAME (event));

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CAPS:
    {
      GstCaps *caps;

      gst_event_parse_caps (event, &caps);
      ret = gst_base_overlay_renderer_setcaps (overlay, caps);
      gst_event_unref (event);
      break;
    }
    case GST_EVENT_SEGMENT:
    {
      const GstSegment *segment;

      GST_DEBUG_OBJECT (overlay, "received new segment");

      gst_event_parse_segment (event, &segment);

      if (segment->format == GST_FORMAT_TIME) {
        gst_segment_copy_into (segment, &overlay->segment);
        GST_DEBUG_OBJECT (overlay, "VIDEO SEGMENT now: %" GST_SEGMENT_FORMAT,
            &overlay->segment);
      } else {
        GST_ELEMENT_WARNING (overlay, STREAM, MUX, (NULL),
            ("received non-TIME newsegment event on video input"));
      }

      ret = gst_pad_event_default (pad, parent, event);
      break;
    }
    case GST_EVENT_EOS:
      GST_BASE_OVERLAY_RENDERER_LOCK (overlay);
      GST_INFO_OBJECT (overlay, "video EOS");
      overlay->eos = TRUE;
      GST_BASE_OVERLAY_RENDERER_UNLOCK (overlay);
      ret = gst_pad_event_default (pad, parent, event);
      break;
    case GST_EVENT_FLUSH_START:
      GST_BASE_OVERLAY_RENDERER_LOCK (overlay);
      GST_INFO_OBJECT (overlay, "video flush start");
      overlay->flushing = TRUE;
      GST_BASE_OVERLAY_RENDERER_BROADCAST (overlay);
      GST_BASE_OVERLAY_RENDERER_UNLOCK (overlay);
      ret = gst_pad_event_default (pad, parent, event);
      break;
    case GST_EVENT_FLUSH_STOP:
      GST_BASE_OVERLAY_RENDERER_LOCK (overlay);
      GST_INFO_OBJECT (overlay, "video flush stop");
      overlay->flushing = FALSE;
      overlay->eos = FALSE;
      gst_segment_init (&overlay->segment, GST_FORMAT_TIME);
      GST_BASE_OVERLAY_RENDERER_UNLOCK (overlay);
      ret = gst_pad_event_default (pad, parent, event);
      break;
    default:
      ret = gst_pad_event_default (pad, parent, event);
      break;
  }

  return ret;
}

static gboolean
gst_base_overlay_renderer_video_query (GstPad * pad, GstObject * parent,
    GstQuery * query)
{
  gboolean ret = FALSE;
  GstBaseOverlayRenderer *overlay;

  overlay = GST_BASE_OVERLAY_RENDERER (parent);

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_CAPS:
    {
      GstCaps *filter, *caps;

      gst_query_parse_caps (query, &filter);
      caps =
          gst_base_overlay_renderer_get_videosink_caps (pad, overlay, filter);
      gst_query_set_caps_result (query, caps);
      gst_caps_unref (caps);
      ret = TRUE;
      break;
    }
    default:
      ret = gst_pad_query_default (pad, parent, query);
      break;
  }

  return ret;
}

static GstFlowReturn
gst_base_overlay_renderer_video_chain (GstPad * pad, GstObject * parent,
    GstBuffer * buffer)
{
  GstBaseOverlayRenderer *overlay;
  GstFlowReturn ret = GST_FLOW_OK;
  gboolean in_seg = FALSE;
  guint64 start, stop, clip_start = 0, clip_stop = 0;
  GstVideoOverlayCompositionMeta *composition_meta;

  overlay = GST_BASE_OVERLAY_RENDERER (parent);

  composition_meta = gst_buffer_get_video_overlay_composition_meta (buffer);
  if (composition_meta) {
    if (overlay->upstream_composition != composition_meta->overlay) {
      GST_DEBUG_OBJECT (overlay, "GstVideoOverlayCompositionMeta found.");
      overlay->upstream_composition = composition_meta->overlay;
    }
  } else if (overlay->upstream_composition != NULL) {
    overlay->upstream_composition = NULL;
  }

  if (!GST_BUFFER_TIMESTAMP_IS_VALID (buffer))
    goto missing_timestamp;

  /* ignore buffers that are outside of the current segment */
  start = GST_BUFFER_TIMESTAMP (buffer);

  if (!GST_BUFFER_DURATION_IS_VALID (buffer)) {
    stop = GST_CLOCK_TIME_NONE;
  } else {
    stop = start + GST_BUFFER_DURATION (buffer);
  }

  GST_LOG_OBJECT (overlay, "%" GST_SEGMENT_FORMAT "  BUFFER: ts=%"
      GST_TIME_FORMAT ", end=%" GST_TIME_FORMAT, &overlay->segment,
      GST_TIME_ARGS (start), GST_TIME_ARGS (stop));

  /* segment_clip() will adjust start unconditionally to segment_start if
   * no stop time is provided, so handle this ourselves */
  if (stop == GST_CLOCK_TIME_NONE && start < overlay->segment.start)
    goto out_of_segment;

  in_seg = gst_segment_clip (&overlay->segment, GST_FORMAT_TIME, start, stop,
      &clip_start, &clip_stop);

  if (!in_seg)
    goto out_of_segment;

  /* if the buffer is only partially in the segment, fix up stamps */
  if (clip_start != start || (stop != -1 && clip_stop != stop)) {
    GST_DEBUG_OBJECT (overlay, "clipping buffer timestamp/duration to segment");
    buffer = gst_buffer_make_writable (buffer);
    GST_BUFFER_TIMESTAMP (buffer) = clip_start;
    if (stop != -1)
      GST_BUFFER_DURATION (buffer) = clip_stop - clip_start;
  }

  /* now, after we've done the clipping, fix up end time if there's no
   * duration (we only use those estimated values internally though, we
   * don't want to set bogus values on the buffer itself) */
  if (stop == -1) {
    if (overlay->info.fps_n && overlay->info.fps_d) {
      GST_DEBUG_OBJECT (overlay, "estimating duration based on framerate");
      stop = start + gst_util_uint64_scale_int (GST_SECOND,
          overlay->info.fps_d, overlay->info.fps_n);
    } else {
      GST_LOG_OBJECT (overlay, "no duration, assuming minimal duration");
      stop = start + 1;         /* we need to assume some interval */
    }
  }

  gst_object_sync_values (GST_OBJECT (overlay), GST_BUFFER_TIMESTAMP (buffer));

  GST_BASE_OVERLAY_RENDERER_LOCK (overlay);

  if (overlay->flushing)
    goto flushing;

  if (overlay->eos)
    goto have_eos;

  GST_BASE_OVERLAY_RENDERER_UNLOCK (overlay);

  // 1. pull overlay meta from buffer

  // 2. render shapes
  if (gst_base_overlay_renderer_render (overlay, buffer))
    ret = gst_base_overlay_renderer_push_frame (overlay, buffer);
  else
    ret = gst_pad_push (overlay->srcpad, buffer);

  /* Update position */
  overlay->segment.position = clip_start;

  return ret;

missing_timestamp:
  {
    GST_WARNING_OBJECT (overlay, "buffer without timestamp, discarding");
    gst_buffer_unref (buffer);
    return GST_FLOW_OK;
  }

flushing:
  {
    GST_BASE_OVERLAY_RENDERER_UNLOCK (overlay);
    GST_DEBUG_OBJECT (overlay, "flushing, discarding buffer");
    gst_buffer_unref (buffer);
    return GST_FLOW_FLUSHING;
  }
have_eos:
  {
    GST_BASE_OVERLAY_RENDERER_UNLOCK (overlay);
    GST_DEBUG_OBJECT (overlay, "eos, discarding buffer");
    gst_buffer_unref (buffer);
    return GST_FLOW_EOS;
  }
out_of_segment:
  {
    GST_DEBUG_OBJECT (overlay, "buffer out of segment, discarding");
    gst_buffer_unref (buffer);
    return GST_FLOW_OK;
  }
}

static GstStateChangeReturn
gst_base_overlay_renderer_change_state (GstElement * element,
    GstStateChange transition)
{
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
  GstBaseOverlayRenderer *overlay = GST_BASE_OVERLAY_RENDERER (element);

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      GST_BASE_OVERLAY_RENDERER_LOCK (overlay);
      overlay->flushing = TRUE;
      GST_BASE_OVERLAY_RENDERER_UNLOCK (overlay);
      break;
    default:
      break;
  }

  ret = parent_class->change_state (element, transition);
  if (ret == GST_STATE_CHANGE_FAILURE)
    return ret;

  switch (transition) {
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      GST_BASE_OVERLAY_RENDERER_LOCK (overlay);
      overlay->flushing = FALSE;
      overlay->eos = FALSE;
      gst_segment_init (&overlay->segment, GST_FORMAT_TIME);
      GST_BASE_OVERLAY_RENDERER_UNLOCK (overlay);
      break;
    default:
      break;
  }

  return ret;
}

static void
gst_base_overlay_renderer_update_render_size (GstBaseOverlayRenderer * overlay)
{
  gdouble video_aspect = (gdouble) overlay->width / (gdouble) overlay->height;
  gdouble window_aspect = (gdouble) overlay->window_width /
      (gdouble) overlay->window_height;

  guint text_buffer_width = 0;
  guint text_buffer_height = 0;

  if (video_aspect >= window_aspect) {
    text_buffer_width = overlay->window_width;
    text_buffer_height = window_aspect * overlay->window_height / video_aspect;
  } else if (video_aspect < window_aspect) {
    text_buffer_width = video_aspect * overlay->window_width / window_aspect;
    text_buffer_height = overlay->window_height;
  }

  if ((overlay->render_width == text_buffer_width) &&
      (overlay->render_height == text_buffer_height))
    return;

  overlay->render_width = text_buffer_width;
  overlay->render_height = text_buffer_height;
  overlay->render_scale = (gdouble) overlay->render_width /
      (gdouble) overlay->width;

  GST_DEBUG_OBJECT (overlay,
      "updating render dimensions %dx%d from stream %dx%d, window %dx%d "
      "and render scale %f", overlay->render_width, overlay->render_height,
      overlay->width, overlay->height, overlay->window_width,
      overlay->window_height, overlay->render_scale);
}

static gboolean
gst_base_overlay_renderer_src_event (GstPad * pad, GstObject * parent,
    GstEvent * event)
{
  GstBaseOverlayRenderer *overlay;
  gboolean ret;

  overlay = GST_BASE_OVERLAY_RENDERER (parent);

  ret = gst_pad_push_event (overlay->sinkpad, event);

  return ret;
}

/* gst_base_overlay_renderer_add_feature_and_intersect:
 *
 * Creates a new #GstCaps containing the (given caps +
 * given caps feature) + (given caps intersected by the
 * given filter).
 *
 * Returns: the new #GstCaps
 */
static GstCaps *
gst_base_overlay_renderer_add_feature_and_intersect (GstCaps * caps,
    const gchar * feature, GstCaps * filter)
{
  int i, caps_size;
  GstCaps *new_caps;

  new_caps = gst_caps_copy (caps);

  caps_size = gst_caps_get_size (new_caps);
  for (i = 0; i < caps_size; i++) {
    GstCapsFeatures *features = gst_caps_get_features (new_caps, i);

    if (!gst_caps_features_is_any (features)) {
      gst_caps_features_add (features, feature);
    }
  }

  gst_caps_append (new_caps, gst_caps_intersect_full (caps,
          filter, GST_CAPS_INTERSECT_FIRST));

  return new_caps;
}

/* gst_base_overlay_renderer_intersect_by_feature:
 *
 * Creates a new #GstCaps based on the following filtering rule.
 *
 * For each individual caps contained in given caps, if the
 * caps uses the given caps feature, keep a version of the caps
 * with the feature and an another one without. Otherwise, intersect
 * the caps with the given filter.
 *
 * Returns: the new #GstCaps
 */
static GstCaps *
gst_base_overlay_renderer_intersect_by_feature (GstCaps * caps,
    const gchar * feature, GstCaps * filter)
{
  int i, caps_size;
  GstCaps *new_caps;

  new_caps = gst_caps_new_empty ();

  caps_size = gst_caps_get_size (caps);
  for (i = 0; i < caps_size; i++) {
    GstStructure *caps_structure = gst_caps_get_structure (caps, i);
    GstCapsFeatures *caps_features =
        gst_caps_features_copy (gst_caps_get_features (caps, i));
    GstCaps *filtered_caps;
    GstCaps *simple_caps =
        gst_caps_new_full (gst_structure_copy (caps_structure), NULL);
    gst_caps_set_features (simple_caps, 0, caps_features);

    if (gst_caps_features_contains (caps_features, feature)) {
      gst_caps_append (new_caps, gst_caps_copy (simple_caps));

      gst_caps_features_remove (caps_features, feature);
      filtered_caps = gst_caps_ref (simple_caps);
    } else {
      filtered_caps = gst_caps_intersect_full (simple_caps, filter,
          GST_CAPS_INTERSECT_FIRST);
    }

    gst_caps_unref (simple_caps);
    gst_caps_append (new_caps, filtered_caps);
  }

  return new_caps;
}

static GstCaps *
gst_base_overlay_renderer_get_videosink_caps (GstPad * pad,
    GstBaseOverlayRenderer * overlay, GstCaps * filter)
{
  GstPad *srcpad = overlay->srcpad;
  GstCaps *peer_caps = NULL, *caps = NULL, *overlay_filter = NULL;

  if (filter) {
    /* filter caps + composition feature + filter caps
     * filtered by the software caps. */
    GstCaps *sw_caps = gst_static_caps_get (&sw_template_caps);
    overlay_filter =
        gst_base_overlay_renderer_add_feature_and_intersect (filter,
        GST_CAPS_FEATURE_META_GST_VIDEO_OVERLAY_COMPOSITION, sw_caps);
    gst_caps_unref (sw_caps);

    GST_DEBUG_OBJECT (overlay, "overlay filter %" GST_PTR_FORMAT,
        overlay_filter);
  }

  peer_caps = gst_pad_peer_query_caps (srcpad, overlay_filter);

  if (overlay_filter)
    gst_caps_unref (overlay_filter);

  if (peer_caps) {

    GST_DEBUG_OBJECT (pad, "peer caps  %" GST_PTR_FORMAT, peer_caps);

    if (gst_caps_is_any (peer_caps)) {
      /* if peer returns ANY caps, return filtered src pad template caps */
      caps = gst_caps_copy (gst_pad_get_pad_template_caps (srcpad));
    } else {

      /* duplicate caps which contains the composition into one version with
       * the meta and one without. Filter the other caps by the software caps */
      GstCaps *sw_caps = gst_static_caps_get (&sw_template_caps);
      caps = gst_base_overlay_renderer_intersect_by_feature (peer_caps,
          GST_CAPS_FEATURE_META_GST_VIDEO_OVERLAY_COMPOSITION, sw_caps);
      gst_caps_unref (sw_caps);
    }

    gst_caps_unref (peer_caps);

  } else {
    /* no peer, our padtemplate is enough then */
    caps = gst_pad_get_pad_template_caps (pad);
  }

  if (filter) {
    GstCaps *intersection = gst_caps_intersect_full (filter, caps,
        GST_CAPS_INTERSECT_FIRST);
    gst_caps_unref (caps);
    caps = intersection;
  }

  GST_DEBUG_OBJECT (overlay, "returning  %" GST_PTR_FORMAT, caps);

  return caps;
}

static GstCaps *
gst_base_overlay_renderer_get_src_caps (GstPad * pad,
    GstBaseOverlayRenderer * overlay, GstCaps * filter)
{
  GstPad *sinkpad = overlay->sinkpad;
  GstCaps *peer_caps = NULL, *caps = NULL, *overlay_filter = NULL;

  if (filter) {
    /* duplicate filter caps which contains the composition into one version
     * with the meta and one without. Filter the other caps by the software
     * caps */
    GstCaps *sw_caps = gst_static_caps_get (&sw_template_caps);
    overlay_filter =
        gst_base_overlay_renderer_intersect_by_feature (filter,
        GST_CAPS_FEATURE_META_GST_VIDEO_OVERLAY_COMPOSITION, sw_caps);
    gst_caps_unref (sw_caps);
  }

  peer_caps = gst_pad_peer_query_caps (sinkpad, overlay_filter);

  if (overlay_filter)
    gst_caps_unref (overlay_filter);

  if (peer_caps) {

    GST_DEBUG_OBJECT (pad, "peer caps  %" GST_PTR_FORMAT, peer_caps);

    if (gst_caps_is_any (peer_caps)) {

      /* if peer returns ANY caps, return filtered sink pad template caps */
      caps = gst_caps_copy (gst_pad_get_pad_template_caps (sinkpad));

    } else {

      /* return upstream caps + composition feature + upstream caps
       * filtered by the software caps. */
      GstCaps *sw_caps = gst_static_caps_get (&sw_template_caps);
      caps = gst_base_overlay_renderer_add_feature_and_intersect (peer_caps,
          GST_CAPS_FEATURE_META_GST_VIDEO_OVERLAY_COMPOSITION, sw_caps);
      gst_caps_unref (sw_caps);
    }

    gst_caps_unref (peer_caps);

  } else {
    /* no peer, our padtemplate is enough then */
    caps = gst_pad_get_pad_template_caps (pad);
  }

  if (filter) {
    GstCaps *intersection;

    intersection =
        gst_caps_intersect_full (filter, caps, GST_CAPS_INTERSECT_FIRST);
    gst_caps_unref (caps);
    caps = intersection;
  }
  GST_DEBUG_OBJECT (overlay, "returning  %" GST_PTR_FORMAT, caps);

  return caps;
}

static void
gst_base_overlay_renderer_set_wrap_mode (GstBaseOverlayRenderer * overlay,
    gint width)
{
  if (overlay->wrap_mode == GST_BASE_OVERLAY_RENDERER_WRAP_MODE_NONE) {
    GST_DEBUG_OBJECT (overlay, "Set wrap mode NONE");
    pango_layout_set_width (overlay->layout, -1);
  } else {
    width = width * PANGO_SCALE;

    GST_DEBUG_OBJECT (overlay, "Set layout width %d", width);
    GST_DEBUG_OBJECT (overlay, "Set wrap mode    %d", overlay->wrap_mode);
    pango_layout_set_width (overlay->layout, width);
  }

  pango_layout_set_wrap (overlay->layout, (PangoWrapMode) overlay->wrap_mode);
}

static void
gst_base_overlay_renderer_adjust_values_with_fontdesc (GstBaseOverlayRenderer *
    overlay, PangoFontDescription * desc)
{
  gint font_size = pango_font_description_get_size (desc) / PANGO_SCALE;
  overlay->shadow_offset = (double) (font_size) / 13.0;
  overlay->outline_offset = (double) (font_size) / 15.0;
  if (overlay->outline_offset < MINIMUM_OUTLINE_OFFSET)
    overlay->outline_offset = MINIMUM_OUTLINE_OFFSET;
}

static void
gst_base_overlay_renderer_get_pos (GstBaseOverlayRenderer * overlay,
    gint * xpos, gint * ypos)
{
  *xpos = (overlay->width - overlay->composition_buffer_width) * overlay->xpos;
  *xpos += overlay->deltax;
  *ypos =
      (overlay->height - overlay->composition_buffer_height) * overlay->ypos;
  *ypos += overlay->deltay;

  GST_DEBUG_OBJECT (overlay, "Placing overlay at (%d, %d)", *xpos, *ypos);
}

static void
gst_base_overlay_renderer_render_rect (GstBaseOverlayRenderer * overlay,
    GstOnnxOverlayGraphicsShape * shape)
{
  cairo_t *cr;
  cairo_surface_t *surface;
  cairo_matrix_t cairo_matrix;
  gint width, height;
  double a, r, g, b;
  GstBuffer *buffer;
  GstMapInfo map;

  cairo_matrix_init_scale (&cairo_matrix, 1, 1);

  /* reallocate overlay buffer */
  width = shape->width;
  height = shape->height;
  buffer = gst_buffer_new_and_alloc (4 * width * height);
  gst_buffer_replace (&overlay->composition_buffer, buffer);
  gst_buffer_unref (buffer);

  gst_buffer_map (buffer, &map, GST_MAP_READWRITE);
  memset (map.data, 0, width * height * 4);

  surface = cairo_image_surface_create_for_data (map.data,
      CAIRO_FORMAT_ARGB32, width, height, width * 4);
  cr = cairo_create (surface);

  /* clear surface */
  cairo_set_operator (cr, CAIRO_OPERATOR_CLEAR);
  cairo_paint (cr);
  cairo_set_operator (cr, CAIRO_OPERATOR_OVER);

  /* apply transformations */
  cairo_set_matrix (cr, &cairo_matrix);
  a = (shape->stroke_color >> 24) & 0xff;
  g = (shape->stroke_color >> 16) & 0xff;
  b = (shape->stroke_color >> 8) & 0xff;
  r = (shape->stroke_color >> 0) & 0xff;

  /* draw text */
  cairo_save (cr);
  cairo_set_source_rgba (cr, r / 255.0, g / 255.0, b / 255.0, a / 255.0);
  cairo_set_line_width (cr, shape->stroke_width);
  cairo_rectangle (cr, 0, 0, width, height);
  cairo_stroke (cr);
  cairo_restore (cr);
  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  gst_buffer_unmap (buffer, &map);
  if (width != 0)
    overlay->composition_buffer_width = width;
  if (height != 0)
    overlay->composition_buffer_height = height;

  gst_base_overlay_renderer_set_composition (overlay, shape);
}

static void
gst_base_overlay_renderer_render_text (GstBaseOverlayRenderer * overlay,
    GstOnnxOverlayGraphicsShape * shape, const gchar * string, gint textlen)
{
  cairo_t *cr;
  cairo_surface_t *surface;
  PangoRectangle ink_rect, logical_rect;
  cairo_matrix_t cairo_matrix;
  gint unscaled_width, unscaled_height;
  gint width, height;
  gboolean full_width = FALSE;
  double scalef_x = 1.0, scalef_y = 1.0;
  double a, r, g, b;
  gdouble shadow_offset = 0.0;
  gdouble outline_offset = 0.0;
  gint xpad = 0, ypad = 0;
  GstBuffer *buffer;
  GstMapInfo map;

  if (overlay->auto_adjust_size) {
    /* 640 pixel is default */
    scalef_x = scalef_y = (double) (overlay->width) / DEFAULT_SCALE_BASIS;
  }

  if (overlay->scale_mode != GST_BASE_OVERLAY_RENDERER_SCALE_MODE_NONE) {
    gint par_n = 1, par_d = 1;

    switch (overlay->scale_mode) {
      case GST_BASE_OVERLAY_RENDERER_SCALE_MODE_PAR:
        par_n = overlay->info.par_n;
        par_d = overlay->info.par_d;
        break;
      case GST_BASE_OVERLAY_RENDERER_SCALE_MODE_DISPLAY:
        /* (width * par_n) / (height * par_d) = (display_w / display_h) */
        if (!gst_util_fraction_multiply (overlay->window_width,
                overlay->window_height, overlay->height, overlay->width,
                &par_n, &par_d)) {
          GST_WARNING_OBJECT (overlay,
              "Can't figure out display ratio, defaulting to 1:1");
          par_n = par_d = 1;
        }
        break;
      case GST_BASE_OVERLAY_RENDERER_SCALE_MODE_USER:
        par_n = overlay->scale_par_n;
        par_d = overlay->scale_par_d;
        break;
      default:
        break;
    }
    /* sanitize */
    if (!par_n || !par_d)
      par_n = par_d = 1;
    /* compensate later scaling as would be done for a par_n / par_d p-a-r;
     * apply all scaling to y so as to allow for predictable text width
     * layout independent of the presentation aspect scaling */
    if (overlay->use_vertical_render) {
      scalef_y *= ((gdouble) par_d) / ((gdouble) par_n);
    } else {
      scalef_y *= ((gdouble) par_n) / ((gdouble) par_d);
    }
    GST_DEBUG_OBJECT (overlay,
        "compensate scaling mode %d par %d/%d, scale %f, %f",
        overlay->scale_mode, par_n, par_d, scalef_x, scalef_y);
  }

  /* This value is uses as cairo line width, which is the diameter of a pen
   * that is circular. That's why only half of it is used to offset */
  if (overlay->draw_outline)
    outline_offset = ceil (overlay->outline_offset);

  pango_layout_set_width (overlay->layout, -1);
  /* set text on pango layout */
  pango_layout_set_markup (overlay->layout, string, textlen);

  /* get subtitle image size */
  pango_layout_get_pixel_extents (overlay->layout, &ink_rect, &logical_rect);

  unscaled_width = ink_rect.width + shadow_offset + outline_offset;
  width = ceil (unscaled_width * scalef_x);

  /*
   * subtitle image width can be larger then overlay width
   * so rearrange overlay wrap mode.
   */
  if (overlay->use_vertical_render) {
    if (width + ypad > overlay->height) {
      width = overlay->height - ypad;
      full_width = TRUE;
    }
  } else if (width + xpad > overlay->width) {
    width = overlay->width - xpad;
    full_width = TRUE;
  }

  if (full_width) {
    unscaled_width = width / scalef_x;
    gst_base_overlay_renderer_set_wrap_mode (overlay,
        unscaled_width - shadow_offset - outline_offset);
    pango_layout_get_pixel_extents (overlay->layout, &ink_rect, &logical_rect);

    unscaled_width = ink_rect.width + shadow_offset + outline_offset;
    width = ceil (unscaled_width * scalef_x);
  }

  unscaled_height = ink_rect.height + shadow_offset + outline_offset;
  height = ceil (unscaled_height * scalef_y);

  if (overlay->use_vertical_render) {
    if (height + xpad > overlay->width) {
      height = overlay->width - xpad;
      unscaled_height = width / scalef_y;
    }
  } else if (height + ypad > overlay->height) {
    height = overlay->height - ypad;
    unscaled_height = height / scalef_y;
  }

  GST_DEBUG_OBJECT (overlay, "Rendering with ink rect (%d, %d) %dx%d and "
      "logical rect (%d, %d) %dx%d", ink_rect.x, ink_rect.y, ink_rect.width,
      ink_rect.height, logical_rect.x, logical_rect.y, logical_rect.width,
      logical_rect.height);
  GST_DEBUG_OBJECT (overlay, "Rendering with width %d and height %d "
      "(shadow %f, outline %f)", unscaled_width, unscaled_height,
      shadow_offset, outline_offset);


  /* Save and scale the rectangles so get_pos() can place the text */
  overlay->ink_rect.x =
      ceil ((ink_rect.x - ceil (outline_offset / 2.0l)) * scalef_x);
  overlay->ink_rect.y =
      ceil ((ink_rect.y - ceil (outline_offset / 2.0l)) * scalef_y);
  overlay->ink_rect.width = width;
  overlay->ink_rect.height = height;

  overlay->logical_rect.x =
      ceil ((logical_rect.x - ceil (outline_offset / 2.0l)) * scalef_x);
  overlay->logical_rect.y =
      ceil ((logical_rect.y - ceil (outline_offset / 2.0l)) * scalef_y);
  overlay->logical_rect.width =
      ceil ((logical_rect.width + shadow_offset + outline_offset) * scalef_x);
  overlay->logical_rect.height =
      ceil ((logical_rect.height + shadow_offset + outline_offset) * scalef_y);

  /* flip the rectangle if doing vertical render */
  if (overlay->use_vertical_render) {
    PangoRectangle tmp = overlay->ink_rect;

    overlay->ink_rect.x = tmp.y;
    overlay->ink_rect.y = tmp.x;
    overlay->ink_rect.width = tmp.height;
    overlay->ink_rect.height = tmp.width;
    /* We want the top left correct, but we now have the top right */
    overlay->ink_rect.x += overlay->ink_rect.width;

    tmp = overlay->logical_rect;
    overlay->logical_rect.x = tmp.y;
    overlay->logical_rect.y = tmp.x;
    overlay->logical_rect.width = tmp.height;
    overlay->logical_rect.height = tmp.width;
    overlay->logical_rect.x += overlay->logical_rect.width;
  }

  /* scale to reported window size */
  width = ceil (width * overlay->render_scale);
  height = ceil (height * overlay->render_scale);
  scalef_x *= overlay->render_scale;
  scalef_y *= overlay->render_scale;

  if (width <= 0 || height <= 0) {
    GST_DEBUG_OBJECT (overlay,
        "Overlay is outside video frame. Skipping text rendering");
    return;
  }

  if (unscaled_height <= 0 || unscaled_width <= 0) {
    GST_DEBUG_OBJECT (overlay,
        "Overlay is outside video frame. Skipping text rendering");
    return;
  }
  /* Prepare the transformation matrix. Note that the transformation happens
   * in reverse order. So for horizontal text, we will translate and then
   * scale. This is important to understand which scale shall be used. */
  /* So, as this init'ed scale happens last, when the rectangle has already
   * been rotated, the scaling applied to text height (up to now),
   * has to be applied along the x-axis */
  if (overlay->use_vertical_render) {
    double tmp;

    tmp = scalef_x;
    scalef_x = scalef_y;
    scalef_y = tmp;
  }
  cairo_matrix_init_scale (&cairo_matrix, scalef_x, scalef_y);

  if (overlay->use_vertical_render) {
    gint tmp;

    /* translate to the center of the image, rotate, and translate the rotated
     * image back to the right place */
    cairo_matrix_translate (&cairo_matrix, unscaled_height / 2.0l,
        unscaled_width / 2.0l);
    /* 90 degree clockwise rotation which is PI / 2 in radiants */
    cairo_matrix_rotate (&cairo_matrix, G_PI_2);
    cairo_matrix_translate (&cairo_matrix, -(unscaled_width / 2.0l),
        -(unscaled_height / 2.0l));

    /* Swap width and height */
    tmp = height;
    height = width;
    width = tmp;
  }

  cairo_matrix_translate (&cairo_matrix,
      ceil (outline_offset / 2.0l) - ink_rect.x,
      ceil (outline_offset / 2.0l) - ink_rect.y);

  /* reallocate overlay buffer */
  buffer = gst_buffer_new_and_alloc (4 * width * height);
  gst_buffer_replace (&overlay->composition_buffer, buffer);
  gst_buffer_unref (buffer);

  gst_buffer_map (buffer, &map, GST_MAP_READWRITE);
  surface = cairo_image_surface_create_for_data (map.data,
      CAIRO_FORMAT_ARGB32, width, height, width * 4);
  cr = cairo_create (surface);

  /* clear surface */
  cairo_set_operator (cr, CAIRO_OPERATOR_CLEAR);
  cairo_paint (cr);

  cairo_set_operator (cr, CAIRO_OPERATOR_OVER);

  /* apply transformations */
  cairo_set_matrix (cr, &cairo_matrix);

  /* FIXME: We use show_layout everywhere except for the surface
   * because it's really faster and internally does all kinds of
   * caching. Unfortunately we have to paint to a cairo path for
   * the outline and this is slow. Once Pango supports user fonts
   * we should use them, see
   * https://bugzilla.gnome.org/show_bug.cgi?id=598695
   *
   * Idea would the be, to create a cairo user font that
   * does shadow, outline, text painting in the
   * render_glyph function.
   */

  /* draw outline text */
  if (overlay->draw_outline) {
    a = (shape->stroke_color >> 24) & 0xff;
    g = (shape->stroke_color >> 16) & 0xff;
    b = (shape->stroke_color >> 8) & 0xff;
    r = (shape->stroke_color >> 0) & 0xff;

    cairo_save (cr);
    cairo_set_source_rgba (cr, r / 255.0, g / 255.0, b / 255.0, a / 255.0);
    cairo_set_line_width (cr, overlay->outline_offset);
    pango_cairo_layout_path (cr, overlay->layout);
    cairo_stroke (cr);
    cairo_restore (cr);
  }

  a = (shape->fill_color >> 24) & 0xff;
  g = (shape->fill_color >> 16) & 0xff;
  b = (shape->fill_color >> 8) & 0xff;
  r = (shape->fill_color >> 0) & 0xff;

  /* draw text */
  cairo_save (cr);
  cairo_set_source_rgba (cr, r / 255.0, g / 255.0, b / 255.0, a / 255.0);
  pango_cairo_show_layout (cr, overlay->layout);
  cairo_restore (cr);

  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  gst_buffer_unmap (buffer, &map);
  if (width != 0)
    overlay->composition_buffer_width = width;
  if (height != 0)
    overlay->composition_buffer_height = height;

  gst_base_overlay_renderer_set_composition (overlay, NULL);
}

static void
render_shape (GstOnnxOverlayGraphicsShape * shape,
    GstBaseOverlayRenderer * self)
{
  self->xpos = (float) shape->x / self->width;
  self->ypos = (float) shape->y / self->height;
  // render shape
  switch (shape->type) {
    case GST_ONNX_OVERLAY_GRAPHICS_SHAPE_RECT:
    {
      GST_DEBUG_OBJECT (self,
          "Extracted shape: rectangle with bounds (%d,%d) %dx%d", shape->x,
          shape->y, shape->width, shape->height);
      self->text = NULL;
      gst_base_overlay_renderer_render_rect (self, shape);
    }
      break;
    case GST_ONNX_OVERLAY_GRAPHICS_SHAPE_TEXT:
    {
      GstOnnxOverlayGraphicsText *text_shape =
          (GstOnnxOverlayGraphicsText *) shape;
      gchar *text_copy;
      const gchar *text = text_shape->text;
      gint textlen = -1;

      GST_DEBUG_OBJECT (self,
          "Extracted shape: text %s with bounds (%d,%d) %dx%d",
          text_shape->text, shape->x, shape->y, shape->width, shape->height);

      /* -1 is the whole string */
      if (text != NULL && textlen < 0) {
        textlen = strlen (text);
      }

      if (text != NULL) {
        text_copy = g_strndup (text, textlen);
      } else {                  /* empty string */
        text_copy = g_strdup (" ");
      }
      g_strdelimit (text_copy, "\r\t", ' ');
      textlen = strlen (text_copy);

      /* FIXME: should we check for UTF-8 here? */
      GST_DEBUG_OBJECT (self, "Rendering '%s'", text_copy);
      self->text = text_copy;
      gst_base_overlay_renderer_render_text (self, shape, text_copy, textlen);

      g_free (text_copy);
    }
      break;
    default:

      break;
  };
}

static gboolean
gst_base_overlay_renderer_render (GstBaseOverlayRenderer * self,
    GstBuffer * buf)
{
  gchar str_buf[5];
  guint num_roi = 0, num_rmeta;
  gpointer state = NULL;

  GST_DEBUG_OBJECT (self, "Begin Render");
  if (self->composition && !self->upstream_composition) {
    gst_video_overlay_composition_unref (self->composition);
    self->composition = NULL;
  }
  num_rmeta = gst_buffer_get_n_meta (buf, GST_ANALYTIC_RELATION_META_API_TYPE);
  GST_TRACE_OBJECT (self, "rmeta:%u", num_rmeta);
  GstAnalyticRelationMeta *rmeta =
      (GstAnalyticRelationMeta *) gst_buffer_get_meta (buf,
      GST_ANALYTIC_RELATION_META_API_TYPE);
  if (rmeta) {
    num_roi = gst_analytic_relation_get_length (rmeta);
    GST_TRACE_OBJECT (self, "rmeta len: %u", num_roi);

    GstAnalyticRelatableMtd *rlt_mtd;
    GQuark rlt_type = gst_analytic_track_mtd_get_type_quark ();
    while ((rlt_mtd =
            gst_analytic_relation_meta_iterate_relatable_filtered (rmeta,
                &state, rlt_type)) != NULL) {
      GstAnalyticTrackMtd *trk_mtd = (GstAnalyticTrackMtd *) rlt_mtd;
      GST_DEBUG_OBJECT (self, "Got track:%lu", trk_mtd->track_id);
      GstAnalyticRelatableMtd *od_rlt_mtd =
          gst_analytic_relation_meta_get_direct_related (rmeta,
          gst_analytic_relatable_mtd_get_id (rlt_mtd),
          GST_ANALYTIC_REL_TYPE_CONTAIN,
          gst_analytic_od_mtd_get_type_quark (),
          NULL);

      if (!od_rlt_mtd)
        continue;

      GstAnalyticODMtd *od_mtd = (GstAnalyticODMtd *) od_rlt_mtd;
      GQuark od_obj_type = gst_analytic_od_mtd_get_type (od_mtd);
      GST_TRACE_OBJECT (self,
          "obj {type: %s track:%lu loc:[(%u,%u)-(%ux%u)] @ %f}",
          g_quark_to_string (od_obj_type),
          trk_mtd->track_id,
          od_mtd->x, od_mtd->y, od_mtd->w, od_mtd->h,
          od_mtd->location_confidence_lvl);

      if ((od_mtd->x > G_MAXUINT16
              || od_mtd->y > G_MAXUINT16
              || od_mtd->w > G_MAXUINT16 || od_mtd->h > G_MAXUINT16)) {
        continue;
      }

      GstAnalyticRelatableMtd *cls_rlt_mtd =
          gst_analytic_relation_meta_get_direct_related (rmeta,
          gst_analytic_relatable_mtd_get_id (
              (GstAnalyticRelatableMtd *) od_mtd),
          GST_ANALYTIC_REL_TYPE_RELATE_TO,
          gst_analytic_cls_mtd_get_type_quark (),
          NULL);

      GstOnnxOverlayGraphicsText textShape;
      GstOnnxOverlayGraphicsRect rectShape;
      rectShape.shape.type = GST_ONNX_OVERLAY_GRAPHICS_SHAPE_RECT;
      rectShape.shape.id = 0;
      rectShape.shape.x = od_mtd->x;
      rectShape.shape.y = od_mtd->y;
      rectShape.shape.width = od_mtd->w;
      rectShape.shape.height = od_mtd->h;
      /* unused */
      rectShape.shape.fill_color = 0;
      rectShape.shape.stroke_width = 5;
      rectShape.shape.stroke_color = self->rect_color;

      render_shape ((GstOnnxOverlayGraphicsShape *) & rectShape, self);

      if (cls_rlt_mtd) {
        g_snprintf (str_buf, sizeof (str_buf), "%04.2f",
            gst_analytic_cls_mtd_get_level (
                (GstAnalyticClsMtd *) cls_rlt_mtd, 0));
      } else {
        g_snprintf (str_buf, sizeof (str_buf), "----");
      }
      gchar *text = g_strdup_printf ("%s (c=%s):%lu",
          g_quark_to_string (od_obj_type),
          str_buf,
          trk_mtd->track_id);

      textShape.shape.type = GST_ONNX_OVERLAY_GRAPHICS_SHAPE_TEXT;
      textShape.shape.id = 0;
      textShape.shape.x = od_mtd->x;
      textShape.shape.y = od_mtd->y;
      textShape.shape.width = od_mtd->w;
      textShape.shape.height = od_mtd->h;
      textShape.shape.fill_color = self->text_color;
      textShape.shape.stroke_width = 5;
      textShape.shape.stroke_color = self->text_outline_color;
      textShape.text = text;
      textShape.font_desc = "Ariel";
      textShape.font_size = 5;

      render_shape ((GstOnnxOverlayGraphicsShape *) & textShape, self);
      g_free (text);
    }
  }
  state = NULL;
  //num_roi = 0;
  //num_roi =
  //    gst_buffer_get_n_meta (buf, GST_VIDEO_REGION_OF_INTEREST_META_API_TYPE);
  //for (i = 0; i < num_roi; i++) {
  //  GstVideoRegionOfInterestMeta *roi;
  //  GstStructure *s;
  //  const gchar *roi_name;

  //  roi = (GstVideoRegionOfInterestMeta *)
  //      gst_buffer_iterate_meta_filtered (buf, &state,
  //      GST_VIDEO_REGION_OF_INTEREST_META_API_TYPE);
  //  if (!roi)
  //    continue;

  //  /* handle roi overflow */
  //  if ((roi->x > G_MAXINT16) || (roi->y > G_MAXINT16)
  //      || (roi->w > G_MAXUINT16) || (roi->h > G_MAXUINT16)) {
  //    GST_DEBUG_OBJECT (self, "Ignoring object with bounds [%d,%d,%d,%d]: "
  //        "overflow", roi->x, roi->y, roi->w, roi->h);
  //    continue;
  //  }
  //  roi_name = g_quark_to_string (roi->roi_type);
  //  if (g_strcmp0 (roi_name, GST_ONNX_OBJECT_DETECTOR_META_NAME) == 0) {
  //    s = gst_video_region_of_interest_meta_get_param (roi,
  //        GST_ONNX_OBJECT_DETECTOR_META_PARAM_NAME);
  //    if (s) {
  //      double score = 0;
  //      const char *label;

  //      GST_LOG_OBJECT (self,
  //          "detected ML meta data: type=%s id=%d (%d, %d) %dx%d",
  //          g_quark_to_string (roi->roi_type), roi->id, roi->x, roi->y, roi->w,
  //          roi->h);

  //      label =
  //          gst_structure_get_string (s,
  //          GST_ONNX_OBJECT_DETECTOR_META_FIELD_LABEL);
  //      GST_DEBUG_OBJECT (self,
  //          "Processing object %s with bounds (%d,%d) %dx%d", label, roi->x,
  //          roi->y, roi->w, roi->h);
  //      if (label
  //          && gst_structure_get_double (s,
  //              GST_ONNX_OBJECT_DETECTOR_META_FIELD_SCORE, &score)) {

  //        GstOnnxOverlayGraphicsText textShape;
  //        GstOnnxOverlayGraphicsRect rectShape;
  //        rectShape.shape.type = GST_ONNX_OVERLAY_GRAPHICS_SHAPE_RECT;
  //        rectShape.shape.id = 0;
  //        rectShape.shape.x = roi->x;
  //        rectShape.shape.y = roi->y;
  //        rectShape.shape.width = roi->w;
  //        rectShape.shape.height = roi->h;
  //        /* unused */
  //        rectShape.shape.fill_color = 0;
  //        rectShape.shape.stroke_width = 5;
  //        rectShape.shape.stroke_color = self->rect_color;

  //        render_shape ((GstOnnxOverlayGraphicsShape *) & rectShape, self);

  //        textShape.shape.type = GST_ONNX_OVERLAY_GRAPHICS_SHAPE_TEXT;
  //        textShape.shape.id = 0;
  //        textShape.shape.x = roi->x;
  //        textShape.shape.y = roi->y;
  //        textShape.shape.width = roi->w;
  //        textShape.shape.height = roi->h;
  //        textShape.shape.fill_color = self->text_color;
  //        textShape.shape.stroke_width = 5;
  //        textShape.shape.stroke_color = self->text_outline_color;
  //        textShape.text = label;
  //        textShape.font_desc = "Ariel";
  //        textShape.font_size = 5;

  //        render_shape ((GstOnnxOverlayGraphicsShape *) & textShape, self);
  //      }
  //    }
  //  }
  //}

  return (num_roi != 0);
}

/* FIXME: should probably be relative to width/height (adjusted for PAR) */
#define BOX_XPAD  6
#define BOX_YPAD  6

static GType
gst_base_overlay_renderer_wrap_mode_get_type (void)
{
  static GType base_overlay_renderer_wrap_mode_type = 0;
  static const GEnumValue base_overlay_renderer_wrap_mode[] = {
    {GST_BASE_OVERLAY_RENDERER_WRAP_MODE_NONE, "none", "none"},
    {GST_BASE_OVERLAY_RENDERER_WRAP_MODE_WORD, "word", "word"},
    {GST_BASE_OVERLAY_RENDERER_WRAP_MODE_CHAR, "char", "char"},
    {GST_BASE_OVERLAY_RENDERER_WRAP_MODE_WORD_CHAR, "wordchar", "wordchar"},
    {0, NULL, NULL},
  };

  if (!base_overlay_renderer_wrap_mode_type) {
    base_overlay_renderer_wrap_mode_type =
        g_enum_register_static ("GstBaseOverlayRendererWrapMode",
        base_overlay_renderer_wrap_mode);
  }
  return base_overlay_renderer_wrap_mode_type;
}

static GType
gst_base_overlay_renderer_line_align_get_type (void)
{
  static GType base_overlay_renderer_line_align_type = 0;
  static const GEnumValue base_overlay_renderer_line_align[] = {
    {GST_BASE_OVERLAY_RENDERER_LINE_ALIGN_LEFT, "left", "left"},
    {GST_BASE_OVERLAY_RENDERER_LINE_ALIGN_CENTER, "center", "center"},
    {GST_BASE_OVERLAY_RENDERER_LINE_ALIGN_RIGHT, "right", "right"},
    {0, NULL, NULL}
  };

  if (!base_overlay_renderer_line_align_type) {
    base_overlay_renderer_line_align_type =
        g_enum_register_static ("GstBaseOverlayRendererLineAlign",
        base_overlay_renderer_line_align);
  }
  return base_overlay_renderer_line_align_type;
}

static GType
gst_base_overlay_renderer_scale_mode_get_type (void)
{
  static GType base_overlay_renderer_scale_mode_type = 0;
  static const GEnumValue base_overlay_renderer_scale_mode[] = {
    {GST_BASE_OVERLAY_RENDERER_SCALE_MODE_NONE, "none", "none"},
    {GST_BASE_OVERLAY_RENDERER_SCALE_MODE_PAR, "par", "par"},
    {GST_BASE_OVERLAY_RENDERER_SCALE_MODE_DISPLAY, "display", "display"},
    {GST_BASE_OVERLAY_RENDERER_SCALE_MODE_USER, "user", "user"},
    {0, NULL, NULL}
  };

  if (!base_overlay_renderer_scale_mode_type) {
    base_overlay_renderer_scale_mode_type =
        g_enum_register_static ("GstBaseOverlayRendererScaleMode",
        base_overlay_renderer_scale_mode);
  }
  return base_overlay_renderer_scale_mode_type;
}

GType
gst_base_overlay_renderer_get_type (void)
{
  static GType type = 0;

  if (g_once_init_enter ((gsize *) & type)) {
    static const GTypeInfo info = {
      sizeof (GstBaseOverlayRendererClass),
      (GBaseInitFunc) NULL,
      NULL,
      (GClassInitFunc) gst_base_overlay_renderer_class_init,
      NULL,
      NULL,
      sizeof (GstBaseOverlayRenderer),
      0,
      (GInstanceInitFunc) gst_base_overlay_renderer_init,
    };

    g_once_init_leave ((gsize *) & type,
        g_type_register_static (GST_TYPE_ELEMENT, "GstBaseOverlayRenderer",
            &info, 0));
  }

  return type;
}
