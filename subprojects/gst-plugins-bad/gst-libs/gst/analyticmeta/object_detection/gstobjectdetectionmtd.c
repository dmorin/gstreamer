/* GStreamer
 * Copyright (C) 2022 Collabora Ltd
 *
 * gstobjecttrackingmtd.c
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstobjectdetectionmtd.h"

#define GST_RELATABLE_MTD_OD_TYPE_NAME "object-detection"

static char relatable_type[] = GST_RELATABLE_MTD_OD_TYPE_NAME;

/**
 * GstAnalyticODMtd:
 * Returns: Quark of #GstAnalyticRelatableMtd type
 */
GQuark
gst_analytic_od_mtd_get_type_quark (void)
{
  return g_quark_from_static_string (relatable_type);
}

/**
 * gst_analytic_od_mtd_get_type_name:
 * Returns: #GstAnalyticRelatableMtd type name.
 */
const gchar *
gst_analytic_od_mtd_get_type_name (void)
{
  return GST_RELATABLE_MTD_OD_TYPE_NAME;
}

/**
 * gst_analytic_od_mtd_get_location:
 * @instance: instance
 * @x: x component of upper-left corner of the object location
 * @y: y component of upper-left corner of the object location
 * @w: bounding box width of the object location
 * @h: bounding box height of the object location
 */
void
gst_analytic_od_mtd_get_location (GstAnalyticODMtd * instance,
    guint * x, guint * y, guint * w, guint * h, gfloat * loc_conf_lvl)
{
  g_assert (instance && x && y && w && h);

  *x = instance->x;
  *y = instance->y;
  *w = instance->w;
  *h = instance->h;

  if (loc_conf_lvl) {
    *loc_conf_lvl = instance->location_confidence_lvl;
  }
}

/**
 * gst_analytic_od_mtd_get_type:
 * @instance: Instance
 * Returns: Quark of class of object associated with this location
 */
GQuark
gst_analytic_od_mtd_get_type (GstAnalyticODMtd * instance)
{
  g_assert (instance);
  return instance->object_type;
}

/**
 * gst_analytic_relation_add_analytic_od_mtd:
 * @instance: Instance of #GstAnalyticRelationMeta where to add classification instance
 * @type: Quark of the object type
 * @x: x component of bounding box upper-left corner
 * @y: y component of bounding box upper-left corner
 * @w: bounding box width
 * @h: bounding box height
 * @loc_conf_lvl: confidence level on the object location
 * Returns: New GstAnalyticODMtd instance added to @instance
 */
GstAnalyticODMtd *
gst_analytic_relation_add_analytic_od_mtd (GstAnalyticRelationMeta * instance,
    GQuark type, guint x, guint y, guint w, guint h, gfloat loc_conf_lvl,
    gsize * new_max_relation_order, gsize * new_max_size)
{
  g_assert (instance);
  GQuark relatable_type = gst_analytic_od_mtd_get_type_quark ();
  gsize size = sizeof (GstAnalyticODMtd);
  GstAnalyticODMtd *od_mtd = (GstAnalyticODMtd *)
      gst_analytic_relation_meta_add_relatable_mtd (instance,
      relatable_type, size, new_max_relation_order, new_max_size);
  if (od_mtd) {
    od_mtd->x = x;
    od_mtd->y = y;
    od_mtd->w = w;
    od_mtd->h = h;
    od_mtd->location_confidence_lvl = loc_conf_lvl;
    od_mtd->object_type = type;
  }
  return od_mtd;
}
