/* GStreamer
 * Copyright (C) 2022 Collabora Ltd
 *
 * gstanalysisclassification.c
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstanalysisclassificationmtd.h"

#define GST_RELATABLE_MTD_CLASSIFICATION_TYPE_NAME "classification"

static char relatable_type[] = GST_RELATABLE_MTD_CLASSIFICATION_TYPE_NAME;

/**
 * GstAnalyticClsMtd:
 * Returns: Quark of #GstAnalyticRelatableMtd type
 */
GQuark
gst_analytic_cls_mtd_get_type_quark (void)
{
  return g_quark_from_static_string (relatable_type);
}

/**
 * gst_analytic_cls_mtd_get_type_name:
 * Returns: #GstAnalyticRelatableMtd type name.
 */
const gchar *
gst_analytic_cls_mtd_get_type_name (void)
{
  return GST_RELATABLE_MTD_CLASSIFICATION_TYPE_NAME;
}

/**
 * gst_analytic_cls_mtd_get_level:
 * @instance: instance
 * @index: Object class index
 * Returns: confidence level for @index
 */
gfloat
gst_analytic_cls_mtd_get_level (GstAnalyticClsMtd * instance, gint index)
{
  g_assert (instance);
  g_assert (index >= 0);
  g_assert (instance->length > index);
  return instance->confidence_levels[index];
}

/**
 * gst_analytic_cls_mtd_get_index_by_label:
 * @instance: instance
 * @label: label of the class
 * Returns: index of @label. <0 if label is not associated
 * with any class of this classification instance
 */
gint
gst_analytic_cls_mtd_get_index_by_label (GstAnalyticClsMtd * instance,
    const char *label)
{
  g_assert (instance);
  g_assert (label);

  for (gint i = 0; i < instance->length; i++) {
    if (g_ascii_strncasecmp (label, instance->labels[i], 15) == 0) {
      return i;
    }
  }
  return -1;
}

/**
 * gst_analytic_cls_mtd_get_index_by_quark:
 * @instance: Instance
 * @quark: Quark of the class
 * Returns: index of the class associated with @quarks ( and label)
 */
gint
gst_analytic_cls_mtd_get_index_by_quark (GstAnalyticClsMtd * instance,
    GQuark quark)
{
  const gchar *label = g_quark_to_string (quark);
  return gst_analytic_cls_mtd_get_index_by_label (instance, label);
}

/**
 * gst_analytic_cls_mtd_get_length:
 * @instance: Instance
 * Returns: Number of classes in this classification instance
 */
gsize
gst_analytic_cls_mtd_get_length (GstAnalyticClsMtd * instance)
{
  return instance->length;
}

/**
 * gst_analytic_cls_mtd_get_quark:
 * @instance: Instance
 * @index: index of the class
 * Returns: Quark of this class (label) associated with @index
 */
GQuark
gst_analytic_cls_mtd_get_quark (GstAnalyticClsMtd * instance, gint index)
{
  g_assert (instance);
  return g_quark_from_string (instance->labels[index]);
}

/**
 * gst_analytic_relation_add_analytic_cls_mtd:
 * @instance: Instance of #GstAnalyticRelationMeta where to add classification instance
 * @confidence_levels: Array (#gfloat) of confidence levels
 * @length: length of @confidence_levels
 * @labels: labels of this classification. Order define index, quark, labels relation
 * Returns: New instance of classification metadata
 */
GstAnalyticClsMtd *
gst_analytic_relation_add_analytic_cls_mtd (GstAnalyticRelationMeta * instance,
    gfloat * confidence_levels, gsize length, const gchar * labels[],
    gsize * new_max_relation_order, gsize * new_max_size)
{
  GQuark relatable_type = gst_analytic_cls_mtd_get_type_quark ();
  g_assert (instance);
  gsize confidence_levels_size = (sizeof (gfloat) * length);
  gsize size = sizeof (GstAnalyticClsMtd) + confidence_levels_size;
  GstAnalyticClsMtd *cls_mtd = (GstAnalyticClsMtd *)
      gst_analytic_relation_meta_add_relatable_mtd (instance,
      relatable_type, size, new_max_relation_order, new_max_size);
  if (cls_mtd) {
    cls_mtd->length = length;
    cls_mtd->labels = (gchar **) labels;
    gpointer confidence_levels_field = &(cls_mtd->confidence_levels);
    memcpy (confidence_levels_field, confidence_levels, confidence_levels_size);
  }
  return cls_mtd;
}
