/* GStreamer
 * Copyright (C) 2022 Collabora Ltd
 *
 * gstobjecttrackingmtd.c
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstobjecttrackingmtd.h"

#define GST_RELATABLE_MTD_TRACK_TYPE_NAME "object-tracking"

static char relatable_type[] = GST_RELATABLE_MTD_TRACK_TYPE_NAME;

/**
 * gst_analytic_track_mtd_get_type_quark:
 * Returns: Quark representing the type of GstAnalyticRelatableMtd
 *
 * Get the quark identifying the relatable type
 */
GQuark
gst_analytic_track_mtd_get_type_quark (void)
{
  return g_quark_from_static_string (relatable_type);
}

/**
 * gst_an_od_mtd_get_type_name:
 * Returns: #GstAnalyticRelatableMtd type name.
 *
 * Get the name identifying relatable type name
 */
const gchar *
gst_analytic_track_mtd_get_type_name (void)
{
  return GST_RELATABLE_MTD_TRACK_TYPE_NAME;
}

/**
 * gst_analytic_track_mtd_update_last_seen:
 * @instance: GstAnalyticTrackMtd instance
 * @last_seen: Timestamp of last time this object was tracked
 */
void
gst_analytic_track_mtd_update_last_seen (GstAnalyticTrackMtd * instance,
    GstClockTime last_seen)
{
  g_return_if_fail (instance);
  instance->track_last_seen = last_seen;
}

/**
 * gst_analytic_track_mtd_set_lost:
 * @instance: Instance of GstAnalyticTrackMtd.  
 *
 * Set tracking to lost
 */
void
gst_analytic_track_mtd_set_lost (GstAnalyticTrackMtd * instance)
{
  g_return_if_fail (instance);
  instance->track_lost = TRUE;
}

/**
 * gst_analytic_relation_add_analytic_track_mtd:
 * @instance: Instance of GstAnalyticRelationMeta where to add tracking mtd
 * @track_id: Tracking id
 * @track_first_seen: Timestamp of first time the object was observed.
 * Returns:(nullable): New GstAnalyticTrackMtd added
 */
GstAnalyticTrackMtd *
gst_analytic_relation_add_analytic_track_mtd (GstAnalyticRelationMeta *
    instance, guint64 track_id, GstClockTime track_first_seen,
    gsize * new_max_relation_order, gsize * new_max_size)
{
  g_return_val_if_fail (instance, NULL);

  GQuark relatable_type = gst_analytic_track_mtd_get_type_quark ();
  gsize size = sizeof (GstAnalyticTrackMtd);
  GstAnalyticTrackMtd *track_mtd = (GstAnalyticTrackMtd *)
      gst_analytic_relation_meta_add_relatable_mtd (instance,
      relatable_type, size, new_max_relation_order, new_max_size);

  track_mtd->track_id = track_id;
  track_mtd->track_first_seen = track_first_seen;
  track_mtd->track_last_seen = track_first_seen;
  track_mtd->track_lost = FALSE;
  return track_mtd;
}
